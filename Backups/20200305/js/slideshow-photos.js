var slides = document.querySelectorAll('#container-slide-B .slide-B');
var currentSlide = 0;
var slideInterval = setInterval(nextSlide,3000);

function nextSlide() {
    slides[currentSlide].className = 'slide-B';
    currentSlide = (currentSlide+1)%slides.length;
    slides[currentSlide].className = 'slide-B showing';
}